#!/bin/bash
qemu-system-riscv64 \
       -nographic \
       -machine virt \
       -bios u-boot-spl.bin \
       -device loader,file=u-boot.itb,addr=0x80200000 \
       $*
