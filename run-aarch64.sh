#!/bin/bash -x

APP_BINARY=$(find target -name plaid)

qemu-system-aarch64 \
    -m 2G \
	-nographic \
	-machine virt \
	-kernel ${APP_BINARY} \
	$*

