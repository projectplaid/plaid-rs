BOARD=aarchv64-virt
TARGET=aarch64-unknown-none
LD_SCRIPT_PATH=$(shell pwd)/src/board/aarch64_virt/
KERNEL_LINKER_SCRIPT=linker.ld
KERNEL_BIN=kernel8.img

RUSTC_MISC_ARGS=
RUSTFLAGS = $(RUSTC_MISC_ARGS)                   \
    -C link-arg=--library-path=$(LD_SCRIPT_PATH) \
    -C link-arg=--script=$(KERNEL_LINKER_SCRIPT)

export LD_SCRIPT_PATH
export RUSTFLAGS

all: 
	cargo build --target=$(TARGET)

.PHONY: all
