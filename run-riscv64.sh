#!/bin/bash

APP_BINARY=$(find target -name plaid)

qemu-system-riscv64 \
    -m 2G \
	-nographic \
	-machine virt \
	-kernel ${APP_BINARY} \
	$*

