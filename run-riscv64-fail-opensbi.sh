#!/bin/bash

APP_BINARY=$(find target -name plaid)

# Create a 128 MB disk image
dd if=/dev/zero of=disk.img bs=1M count=128
sudo parted disk.img mklabel gpt
# Attach disk.img with the first available loop device
sudo losetup --find --show disk.img

# Create a couple of primary partitions
sudo parted --align minimal /dev/loop0 mkpart primary fat32 0 100%

sudo mkfs.vfat /dev/loop0p1

# Mark first partition as bootable
sudo parted /dev/loop0 set 1 boot on

MNTDIR=$(mktemp -d)

# Mount the 1st partition
sudo mount /dev/loop0p1 ${MNTDIR}

# Copy the Linux kernel
sudo cp $APP_BINARY ${MNTDIR}/plaid.elf

cat <<EOF | sudo tee ${MNTDIR}/boot.scr
fatload virtio 0:1 0x84000000 plaid.elf
bootelf -p 0x84000000
EOF

# Unmount the partition to save the changes
sudo umount ${MNTDIR}

sudo rm -rf ${MNTDIR}

sudo losetup -d /dev/loop0

qemu-system-riscv64 \
    -m 2G \
    -machine virt \
    -bios u-boot/riscv64/u-boot-spl.bin \
    -device loader,file=u-boot/riscv64/u-boot.itb,addr=0x80200000 \
    -blockdev driver=file,filename=./disk.img,node-name=disk \
    -device virtio-blk-device,drive=disk \
    -nographic \
    $*