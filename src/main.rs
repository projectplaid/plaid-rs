#![no_std]
#![no_main]
#![feature(allocator_api)]

extern crate spin;
extern crate talc;

use talc::*;

#[cfg(feature = "riscv64")]
use riscv_rt::entry;

#[macro_use]
pub mod debug;
pub mod arch;
pub mod board;
pub mod panic_handler;
pub mod syscalls;

extern "C" {
    static __heap_start: u8;
}
static _heap_size: usize = 16 * 1024 * 1024;

#[no_mangle]
unsafe fn kernel_init() -> ! {
    let _allocator = unsafe {
        Talc::new(ClaimOnOom::new(Span::from_base_size(
            &__heap_start as *const _ as *mut _,
            _heap_size,
        )))
        .lock::<spin::Mutex<()>>()
    };

    arch::cpu::init();
    arch::mmu::init();

    arch::exceptions::enable_all();

    println!("Interrupts are enabled");

    loop {
        // your code goes here
        // if cfg!(feature = "riscv64") {
        //     println!("Now we WFI");
        //     unsafe {
        //         riscv::asm::wfi();
        //     }
        // }
    }
}

#[cfg(target_arch = "riscv64")]
#[entry]
fn main(_a0: usize, _a1: usize, _a2: usize) -> ! {
    // a0 = mhart
    // a1 = fdt address
    kernel_init()
}
