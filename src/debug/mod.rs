use core::{future::Future, pin::Pin, task::Poll};

#[macro_export]
macro_rules! print {
    ($fmt:literal$(, $($arg: tt)+)?) => {
        $crate::debug::print_args(format_args!($fmt $(,$($arg)+)?))
    }
}

#[macro_export]
macro_rules! println {
    ($fmt:literal$(, $($arg: tt)+)?) => {{
        $crate::print!($fmt $(,$($arg)+)?);
        $crate::debug::print("\n");
    }};
    () => {
        $crate::debug::print("\n");
    }
}

struct Writer {}

pub fn print_args(t: core::fmt::Arguments) {
    use core::fmt::Write;
    let mut writer = Writer {};
    writer.write_fmt(t).unwrap();
}

impl core::fmt::Write for Writer {
    fn write_str(&mut self, s: &str) -> core::fmt::Result {
        print(s);
        Ok(())
    }
}

#[cfg(target_arch = "riscv64")]
fn getc() -> Option<u8> {
    sbi::legacy::console_getchar()
}

#[cfg(target_arch = "riscv64")]
fn putc(c: u8) {
    sbi::legacy::console_putchar(c)
}

#[cfg(target_arch = "aarch64")]
fn getc() -> Option<u8> {
    None
}

#[cfg(target_arch = "aarch64")]
fn putc(c: u8) {}

pub fn print(t: &str) {
    t.chars().for_each(|c| {
        let c: u8 = c.try_into().unwrap_or(b'?');
        putc(c)
    });
}

pub fn read() -> Option<u8> {
    getc()
}

struct DebugConsole();
impl Future for DebugConsole {
    type Output = u8;

    fn poll(self: Pin<&mut Self>, cx: &mut core::task::Context<'_>) -> Poll<Self::Output> {
        match getc() {
            Some(c) => Poll::Ready(c),
            None => {
                cx.waker().wake_by_ref();
                Poll::Pending
            }
        }
    }
}

pub async fn get_char() -> u8 {
    DebugConsole().await
}
