use crate::println;
use core::{hint::unreachable_unchecked, panic::PanicInfo};

#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    println!("{info}");

    crate::arch::cpu::reset();

    unsafe {
        println!("System reset failed");
        unreachable_unchecked(); // this can pretty much only happen if there is a bug in the sbi implementation or if sbi is not present, unreachable_unchecked so we don't panic again
    }
}
