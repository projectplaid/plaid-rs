use riscv::register::*;

pub fn enable_all() {
    unsafe {
        sie::set_stimer();
        sie::set_sext();
        sie::set_ssoft();
        sstatus::set_sie();
    }
}

pub fn disable_all() {
    unsafe {
        sstatus::clear_sie();
    }
}

#[export_name = "MachineTimer"]
fn custom_mtimer_handler() {}

#[export_name = "SupervisorTimer"]
fn custom_stimer_handler() {}

#[export_name = "DefaultHandler"]
fn default_interrupt_handler() {
    let scause = scause::read();

    println!("scause details");
    println!("bits = {}", scause.bits());
    println!("code = {}", scause.code());
    println!("interrupt? {}", scause.is_interrupt());
    println!("exception? {}", scause.is_exception());

    println!();

    panic!("Stopping after exception handler");
}
