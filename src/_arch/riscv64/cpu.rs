// RISC-V 64 specific code

use sbi::system_reset::{ResetReason, ResetType};

pub fn reset() {
    let _ = sbi::system_reset::system_reset(ResetType::Shutdown, ResetReason::SystemFailure);
}

pub fn init() {
    println!("UART initialized and an allocator!\n\r");

    let fdt = unsafe { fdt::Fdt::from_ptr(_a1 as *const u8).unwrap() };

    println!("Loaded FDT\n");
    println!("Model = {}\n", fdt.root().model());

    let ver = sbi::base::spec_version();

    println!("SBI Version major = {} minor = {}", ver.major, ver.minor);

    let t = riscv::register::time::read();
    println!("current time register = {}", t);
    let t_result = sbi::timer::set_timer(t as u64 + 1000);
    match t_result {
        Ok(_) => {
            println!("timer set")
        }
        Err(_) => {
            println!("error setting timer");
        }
    }
}
