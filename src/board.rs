// board abstractions

cfg_if::cfg_if! {
    if #[cfg(feature = "riscv64-virt")] {
        mod riscv64_virt;
        pub use riscv64_virt::*;
    } else if #[cfg(feature = "aarch64-virt")] {
        mod aarch64_virt;
        pub use aarch64_virt::*;
    }
}
