/* arch specific module */

cfg_if::cfg_if! {
    if #[cfg(target_arch = "riscv64")] {
        #[path = "_arch/riscv64/cpu.rs"]
        #[macro_use]
        pub mod cpu;

        #[path = "_arch/riscv64/exceptions.rs"]
        #[macro_use]
        pub mod exceptions;

        #[path = "_arch/riscv64/mmu.rs"]
        #[macro_use]
        pub mod mmu;
    } else if #[cfg(target_arch = "aarch64")] {
        #[path = "_arch/aarch64/cpu.rs"]
        #[macro_use]
        pub mod cpu;

        #[path = "_arch/aarch64/exceptions.rs"]
        #[macro_use]
        pub mod exceptions;

        #[path = "_arch/aarch64/mmu.rs"]
        #[macro_use]
        pub mod mmu;
    } else {
        compile_error!("support for architecture not implemented");
    }
}
